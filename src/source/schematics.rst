Schematics
++++++++++

All of our Reform-related schematics, design files and documentation are publicly available on our GitLab instance:

`<https://source.mnt.re/reform/reform>`_

All folders that have "pcb" in their name contain KiCad projects that you can explore interactively.

.. role:: raw-latex(raw)
   :format: latex

:raw-latex:`\small`

Motherboard Schematics
======================

- `PDF: Motherboard Schematics <_static/schem/reform2-motherboard25.pdf>`_
- `KiCad: Motherboard Design Files <https://source.mnt.re/reform/reform/-/tree/master/reform2-motherboard25-pcb>`_

:raw-latex:`\includepdf[pages=1,angle=0]{_static/schem/reform2-motherboard25.pdf}`
:raw-latex:`\includepdf[pages=2,angle=90]{_static/schem/reform2-motherboard25.pdf}`
:raw-latex:`\includepdf[pages=3,angle=90]{_static/schem/reform2-motherboard25.pdf}`
:raw-latex:`\includepdf[pages=4,angle=90]{_static/schem/reform2-motherboard25.pdf}`
:raw-latex:`\includepdf[pages=5,angle=90]{_static/schem/reform2-motherboard25.pdf}`
:raw-latex:`\includepdf[pages=6,angle=90]{_static/schem/reform2-motherboard25.pdf}`
:raw-latex:`\includepdf[pages=7,angle=90]{_static/schem/reform2-motherboard25.pdf}`
:raw-latex:`\includepdf[pages=8,angle=90]{_static/schem/reform2-motherboard25.pdf}`
:raw-latex:`\includepdf[pages=9,angle=90]{_static/schem/reform2-motherboard25.pdf}`
:raw-latex:`\includepdf[pages=10,angle=90]{_static/schem/reform2-motherboard25.pdf}`
:raw-latex:`\includepdf[pages=11,angle=90]{_static/schem/reform2-motherboard25.pdf}`
:raw-latex:`\includepdf[pages=12,angle=90]{_static/schem/reform2-motherboard25.pdf}`

Motherboard Bill of Materials
=============================

.. csv-table::
   :file: _static/bom/reform2-motherboard25.csv
   :header-rows: 1
   :widths: 24,3,9,24,45

Keyboard Schematics
===================

- `PDF: Keyboard V3 Schematics <_static/schem/reform2-keyboard3.pdf>`_
- `KiCad: Keyboard V3 Design Files <https://source.mnt.re/reform/reform/-/tree/master/reform2-keyboard3-pcb>`_

:raw-latex:`\includepdf[pages=1,angle=90]{_static/schem/reform2-keyboard3.pdf}`
:raw-latex:`\includepdf[pages=2,angle=90]{_static/schem/reform2-keyboard3.pdf}`
:raw-latex:`\includepdf[pages=3,angle=90]{_static/schem/reform2-keyboard3.pdf}`

Keyboard Bill of Materials
==========================

.. csv-table::
   :file: _static/bom/reform2-keyboard3.csv
   :header-rows: 1
   :widths: 24,3,9,24,45

OLED Schematics
===============

- `PDF: OLED Schematics <_static/schem/reform2-oled.pdf>`_
- `KiCad: Keyboard Design Files <https://source.mnt.re/reform/reform/-/tree/master/reform2-oled-pcb>`_

:raw-latex:`\includepdf[pages=1,angle=90]{_static/schem/reform2-oled.pdf}`

.. csv-table::
   :file: _static/bom/reform2-oled.csv
   :header-rows: 1
   :widths: 24,3,9,24,45

Trackball Schematics
====================

- `PDF: Trackball Schematics <_static/schem/reform2-trackball2.pdf>`_
- `KiCad: Trackball Design Files <https://source.mnt.re/reform/reform/-/tree/master/reform2-trackball2-pcb>`_

:raw-latex:`\includepdf[pages=1,angle=90]{_static/schem/reform2-trackball2.pdf}`

Trackball Bill of Materials
===========================

.. csv-table::
   :file: _static/bom/reform2-trackball2.csv
   :header-rows: 1
   :widths: 24,3,9,24,45

Trackball Sensor Schematics
===========================

- `PDF: Trackball Sensor Schematics <_static/schem/reform2-trackball-sensor.pdf>`_
- `KiCad: Trackball Sensor Design Files <https://source.mnt.re/reform/reform/-/tree/master/reform2-trackball-sensor-pcb>`_

:raw-latex:`\includepdf[pages=1,angle=90]{_static/schem/reform2-trackball-sensor.pdf}`

Trackball Sensor Bill of Materials
==================================

.. csv-table::
   :file: _static/bom/reform2-trackball-sensor.csv
   :header-rows: 1
   :widths: 24,3,9,24,45

Trackpad Schematics
===================

- `PDF: Trackpad Schematics <_static/schem/reform2-trackpad.pdf>`_
- `KiCad: Trackpad Design Files <https://source.mnt.re/reform/reform/-/tree/master/reform2-trackpad-pcb>`_

:raw-latex:`\includepdf[pages=1,angle=90]{_static/schem/reform2-trackpad.pdf}`

Trackpad Bill of Materials
==========================

.. csv-table::
   :file: _static/bom/reform2-trackpad.csv
   :header-rows: 1
   :widths: 24,3,9,24,45

Battery Pack Schematics
=======================

- `PDF: Protected Battery Pack Schematics <_static/schem/reform2-protected-batterypack.pdf>`_
- `KiCad: Protected Battery Pack Design Files <https://source.mnt.re/reform/reform/-/tree/master/reform2-protected-batterypack-pcb>`_

:raw-latex:`\includepdf[pages=1]{_static/schem/reform2-protected-batterypack.pdf}`

Battery Pack Bill of Materials
==============================

.. csv-table::
   :file: _static/bom/reform2-protected-batterypack.csv
   :header-rows: 1
   :widths: 24,3,9,24,45

Assembly Parts
==============

.. csv-table::
   :file: _static/bom/reform2-other-parts.csv
   :header-rows: 1
   :widths: 40,5,25,30

:raw-latex:`\normalsize`

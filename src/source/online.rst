Online Resources
================

Get the latest news and additional resources for MNT Reform at:

- The MNT Research website: `<https://mntre.com>`_
- The MNT Community forum: `<https://community.mnt.re>`_
- Source code repositories (including electronics design files and 3D models for printing and laser cutting): `<https://source.mnt.re/reform>`_
- MNT Research on Crowd Supply: `<https://www.crowdsupply.com/mnt>`_
- MNT Research Shop: `<https://shop.mntre.com>`_
- For email support, contact: support@mntre.com
- Modularity explained (including a comparison table of Processor Modules): `<https://mntre.com/modularity.html>`_
- PDF and web versions of manuals, including this handbook: `<https://mntre.com/docs-reform.html>`_
- iFixit Maintenance Guides for MNT Reform: `<https://www.ifixit.com/Device/MNT_Reform>`_

You can join fellow MNT Reform enthusiasts in the official IRC channel ``#mnt-reform`` on ``irc.libera.chat``.

Discover more about the main software building blocks of the MNT Reform system:

- Debian GNU/Linux: `<https://debian.org>`_
- U-Boot: `<https://docs.u-boot.org/en/latest/>`_
- Sway: `<https://swaywm.org>`_
- Wayfire: `<https://wayfire.org>`_

Credits
=======

.. role:: raw-latex(raw)
   :format: latex

The MNT Reform Operator Handbook, Second Edition. Berlin, March 2024.
Written by Lukas F. Hartmann and Anri Paul Hennies. Published by MNT Research GmbH.

:Concept, Electronics, Software: Lukas F. Hartmann

:Industrial Design: Ana Beatriz Albertini Dantas

:Quality & Assembly, Sleeve Design: Greta Melnik

:Handbook Design: 100rabbits, Lukas F. Hartmann

:Additional Editing: Brendan Nystedt, Liaizon Wakest, Johannes Schauer Marin Rodrigues

:3D Illustration: Paul Klingberg

:Accounting: Christian 'plomlompom' Heller

:Debian Integration & Tooling: Johannes Schauer Marin Rodrigues

:Contributions & Thanks: Patrick Wildt, Sigrid Solveig Haflínudóttir, Cinap Lenrek, Stanley Lieber, Dirk Eibach, Jacqueline Leykam, Neil Armstrong, John Bliss, Chartreuse, Norman Feske & Stefan Kalkowski (Genode), Valtteri Koskivuori, Jack Humbert, Nanocodebug, Neelfyn, Maximilian Voigt, Helen Leigh, Daniel Amor, Sergio Amor, F. Schneider, Zoé & Elen (Fully Automated), Boundary Devices, Timonsku, Lukas Stach, Marek Vasut, Christian Gmeiner, Daniel Stone, Simon Ser, Chris Healy, Drew Fustini, Jonas Haring, Philip Steffan, Florian Hadler (Interface Critique), Felix Bayer, Li0n, Schneider, Sven Gebhardt, Jan Varwig, Gabriel Yoran, Martin Meyerhoff, GyrosGeier, Philipp Dikmann, Michael Christophersson, Markus Angermeier

:Therapy Dog: Tina

:Crowdfunding Partner: Crowd Supply

:Additional Funding: NLNet

:Printed Circuit Boards: PCBWay, JLCPCB

:Milling: JPR

:Inter Font: @rsms

:Lab Tools: Rigol DS1054Z, Fluke 117, Weller WD-1, Puhui T-962C, AmScope SM-4T, BCN3D Sigma R17, Formlabs Form2, Epilog Mini 18, Prusa i3 MK3S+

:Software: Debian GNU/Linux, KiCad, Inkscape, GIMP, Emacs, Vim, GitLab, LUFA, MicroBuilder, Blender, FreeCAD, OpenSCAD, Fusion, Cura, Sphinx, Pandoc, XeTeX, Scribus

| © 2018-2024 MNT Research GmbH, Fehlerstr. 8, 12161 Berlin
| Managing Director: Lukas F. Hartmann
| Registergericht: Amtsgericht Charlottenburg
| Aktenzeichen: HRB 136605 B
| VAT ID: DE278908891
| WEEE: DE 33315564
| Web: https://mntre.com

MNT Reform is Open Source Hardware certified by OSHWA (UID DE000017).

The sources for this handbook and most MNT Reform software components (check repositories for details) are licensed under GNU GPL v3 or newer. The artwork is licensed under CC BY-SA 4.0. The MNT Reform hardware design files are licensed under CERN-OHL-S 2.0. The MNT symbol is a registered trademark of MNT Research GmbH.

.. image:: _static/illustrations/pinguin-druck.png
   :width: 30%

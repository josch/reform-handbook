#!/bin/bash

for f in motherboard25 keyboard3 oled trackball2 trackball-sensor trackpad protected-batterypack
do
    echo =====================================================
    echo $f
    echo =====================================================
    ruby source/scripts/cleanup-bom.rb ~/src/mref/reform/reform2-$f-pcb/reform2-$f.csv source/_static/bom/reform2-$f.csv
    echo =====================================================
done

#!/bin/bash

cd source

PNGS=$(grep "\\-icon.png" *.rst | cut -d ' ' -f 3)
for DEST in $PNGS
do
    ORIG=${DEST//\-icon/}
    cp $ORIG $DEST
done

cd _inkscape
./make-pngs.sh
cd ..

cd ..

make clean
make html

# remove alabaster css
echo "" > build/html/_static/alabaster.css

# remove printer logo from web version
mv build/html/credits.html build/html/temp.html
grep -v pinguin <build/html/temp.html >build/html/credits.html
rm build/html/temp.html

# remove image sources
rm -rf build/html/_static/illustrations

#!/bin/bash

set -e
set -x

# requires inkscape
./make-eps-illus.sh
cp ./source/_static/illustrations/pinguin/* ./source/_static/illustrations/

cd source

# generate content tex files from individual rst sources

cat _tex/pre.tex >reform-handbook.tex

for section in \
  toc \
  preface \
  safety \
  quickstart \
  input \
  linux \
  desktops \
  software \
  hardware \
  advanced \
  schematics \
  online \
  credits
do
  cat $section.rst | grep -v '`KiCad: ' | grep -v '`PDF: ' | pandoc -o _$section.tex -frst+smart --verbose -V fontsize=10pt --top-level-division=chapter
  cat _tex/section.tex >>reform-handbook.tex
  sed 's/\(-icon\)\.png/\.icon/g' _$section.tex >>reform-handbook.tex
done

sed -i 's/\.png/\.eps/g' reform-handbook.tex
sed -i 's/\.icon/\.png/g' reform-handbook.tex

cat _tex/post.tex >>reform-handbook.tex

# generate reform-handbook.pdf
xelatex --interaction=nonstopmode reform-handbook.tex
# thrice to get the TOC right
xelatex --interaction=nonstopmode reform-handbook.tex
xelatex --interaction=nonstopmode reform-handbook.tex

# clean up
mv reform-handbook.pdf ../build/
rm -- _*.tex
rm -- *.out
rm -- *.log
rm -- *.aux
rm -- *.toc

cd ..

echo ">>> generated ./build/reform-handbook.pdf <<<"
